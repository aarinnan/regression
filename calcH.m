function newmod = calcH( mod)
%newmod = calcH( mod)
% Calculates the leverage of the model given. If mod is from 'cv' then the
% Hotelling for both the calibration and the validation is calculated in
% the output, and given as H in the structure.
%
%INPUT:
% mod     From one of the pls-algorithms or 'cv'
%
%OUTPUT:
% newmod  The same struct as mod, but now also including the leverage, H
%
%See also: apls, simbpls, bipls, cv

%AAR 180612

%Check if the input is from 'cv' or "just" a regular PLS
if isfield( mod, 'Cal')
    newmod = mod;
    clear mod
    for cf = 1:size( newmod.Cal.T, 2)
        tc = newmod.Cal.T( :, 1:cf);
        tv = newmod.Val.T( :, 1:cf);
        newmod.Cal.H( :, cf) = diag( tc * inv( tc' * tc) * tc');
        newmod.Val.H( :, cf) = diag( tv * inv( tv' * tv) * tv');
    end
else
    error( 'Not made for anything but ''cv'' yet')
end
    