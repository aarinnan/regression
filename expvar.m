function a=expvar(X,t,p)
%a=expvar(X,t,p)
%
% Explained variance from PCA or PLS. Can handle missing values
%
% INPUT:
%  X  Data-matrix (same as used for PCA/ PLS)
%  t  Scores from PCA/ PLS (X-scores)
%  p  Loadings from PCA/ PLS (X-loadings)
%
% OUTPUT:
%  a  Explained variance. Three columns, one for the current component, 
%      one for the total explained variance and the last gives the
%      eigenvalues
%
% See also: apls, npca

% 170222 AAR Changed it from variance to squared values (as is correct!)
% 150210 AAR Added the eigenvalues as well
% 030805 AAR

nLV=min(size(t,2),size(p,2));
if size(t,1)~=size(X,1)
    disp('''X'' and ''t'' do not have the same number of rows')
    return
end

if size(p,1)~=size(X,2)
    disp('''X'' do not have the same number of columns as ''p'' has rows')
    return
end
    
totvar = nansum( vec( X.^2) );
a=[0 0];
for i=1:nLV
    a( i+1, 2) = 1 - nansum( vec( ( X - t( :, 1:i) * p( :, 1:i)').^2) )/totvar;
    a( i+1, 1) = a( i+1, 2) - a( i, 2);
end
if a( 2, 1) < 0
    disp( 'It seems that the ''X'' and the ''t'' & ''p'' are not the correct ones')
end
a=a(2:end,:);
pn = sqrt( diag( p' * p) );
t = t * diag( pn);
a( :, end+1) = diag( (t'*t) )/( size( t, 1) - 1);