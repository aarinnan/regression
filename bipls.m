function model = bipls( x, y, nLV, nrm)
%model = bipls( x, y, nLV, nrm)
% Fitting the PLS by using the bidiagonal algorithm
%
%INPUT:
% x
% y    
% nLV  Number of factors to estimate
% nmr  Normalize the loadings to length 1 (default = false)
%
%OUTPUT:
% model  Structure containing all the model parameters
%
%See also: apls

%Based on: Martin Andersson: A comparison of nine PLS1 algorithms, Journal
%           of Chemometrics 2009; 23: 518-529
%Adjusted according to Ulf Indahl (unpublished work)
 
if nargin < 4
    nrm = false;
end

w(:,1) = x'*y;
w(:,1) = w(:,1)/ sqrt( w(:,1)' * w( :, 1) );
t(:,1) = x*w(:,1);
B(1,1) = sqrt( t( :, 1)' * t( :, 1) );
t(:,1) = t(:,1)/B(1,1);

nLV = min( [ size( x) nLV]); %Close to being the rank for all chemical data

for a = 2:nLV
    w(:,a) = x'*t(:,a-1)-B(a-1,a-1)*w(:,a-1);
    
    %Need to re-orthogonalize w
    corr =  w( :, a)' * w( :, 1:a-1);
    w( :, a) = w( :, a) - w( :, 1:a-1) * corr'; 

    B(a-1,a) = sqrt( w( :, a)' * w( :, a) );
    w(:,a) = w(:,a)/B(a-1,a);
    t(:,a) = x*w(:,a)-B(a-1,a)*t(:,a-1);
    
    %Need to re-orthogonalize t
    corr = t( :, a)' * t( :, 1:a-1);
    t( :, a) = t( :, a) - t( :, 1:a-1) * corr';
    
    B(a,a) = sqrt( t( :, a)' * t( :, a) );
    t(:,a) = t(:,a)/B(a,a);
end
invB = pinv(B);
q = y'*t;

% %A rough reduction of the nLVs
% bd = diag( B);
% temp = bd/ bd(1);
% nLV = min( find( temp < sqrt( eps), 1), nLV);

%Put the length of t back into t (as is normally done), and calculate p
told = t;
t = t * diag( diag( B));
p = ( pinv( t) * x)';

for a = 1:nLV
    b(:,a) = w(:,1:a)*(invB(1:a,1:a)*q(1:a)');
    u( :, a) = y/ q(a);
    y = y - told( :, a) * q(a);
end

q = q * inv( diag( diag( B) ) );

if nrm
    pnorm = sqrt( diag( p' * p) );
    t = t * diag( pnorm);
    w = w * diag( pnorm);
    p = p * diag( 1./ pnorm);
end

model.Tx = t;
model.Px = p;
model.W = w;
model.B = b;
model.Ty = u;
model.Py = q;