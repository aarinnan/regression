function [nLV, test] = estfac( mod, opt)
%[nLV, test] = estfac( mod, opt)
%
%Estimate the correct number of factors from a PLS-model
%INPUT:
% mod   The output from 'cv'
% opt   Six numbers
%        1 - sign. diff of slope of RMS   , default = 0.15
%        2 - min accepted diff in RMS     , default = 0.25
%        3 - sign. diff in B              , default = 0.05
%        4 - size of "hill" allowed       , default = 2
%        5 - accepted rel. std of B       , default = 3
%        6 - accepted degree of non-smooth, default = 5
%
%OUTPUT:
% nLV   Estimated number of factors
%
% See also: cv

%261111 AAR 'rep' has been added as the output from 'cv'
%220611 AAR Added some more features, but only on a test basis, so more
%            outputs are given if wanted. Only gives "basic" solution if
%            one output is wanted.
%290111 AAR

if nargin < 2
    opt = [.15 .25 .05 2 3 5];
end

rmstot = mod.Val.rms;

%As I now am in a testing period, I would like to get all the possible
%outputs from this algoritm. Therefore all the "weird" coding
if length( rmstot) < 3
    nLV = 1;
else
    rms = rmstot;
    rmstot(1) = []; %Remove the 0-factor model
    %Find local (and global) minima
    d1 = diff( rms);
    %Find the break-points
    d2 = diff( rms, 2);
    %Have to find where this is not significantly different from 0!
    test = d2./d1( 1:end-1);
    d2( test > - opt(1)) = - abs( d2( test > - opt(1)));
    [temp, nLV] = min( rms (2:end));
    nLV = [nLV find( d2 < 0) - 1];
    %Remove those which are neighbours, selecting the most conservative one
    nLV( find( diff( nLV) == 1) + 1) = [];
    nLV = fjernlike( sort( [nLV'; find( d1' > 0) - 1]) );
    
    %Remove the 0 factor solution
    nLV( nLV == 0) = [];
    
    rms = rms( nLV + 1);
    if length( nLV) > 1
        %Remove those which are after the global minima
        [temp, id] = min( rms);
        rms = rms( 1:id);
        nLV = nLV( 1:id);
        
        %Remove those which are part of a "hill"
        id = true( length( nLV), 1);
        for cf = 2:length( nLV)
            if rms( cf) > min( rms( 1:cf-1) )
                id( cf) = false;
            end
        end
        nLV = nLV( id);
        rms = rms( id);
        
        %Check if there is a significant difference between the regression
        %coefficients. Remove those where there is no difference
        nLVx = nLV;
        rmsx = rms;
        id = true( length( nLVx), 1);
        cf = 2;
        while cf <= length( nLVx)
            [h, sig] = ttest( abs( mod.Cal.B( :, nLV( cf) ) - mod.Cal.B( :, nLV( cf - 1) ) ) );%, 0, opt(3), 1);
            if sig > opt(3)
                nLVx( cf) = [];
                rmsx( cf) = [];
            else
                cf = cf + 1;
            end
        end
        
        nLVh = nLV;
        nLVm = nLV;
        nLVs = [1; nLV];
        if length( nLV) > 1
            %Should also investigate if there is a big "hill" between two nLV
            %estimates in the RMSE-curve. That "hill" should not be crossed
            cf = 2;
            rmsh = rms;
            test = zeros( length( nLV), 1 );
            for cf = 2:length( nLV)
                test( cf) = sum( rmstot( nLV( cf - 1) + 1:nLV( cf) ) > rmstot( nLV( cf - 1) ) );
            end
            nLVh( test > opt(4) ) = [];
            rmsh( test > opt(4) ) = [];
            clear temp
            temp{1} = test;
            
            %The standard deviation of the regression coefficients should not
            %be too high
            clear test
            for cf = 1:length( nLVs)
                test( cf, :) = std( mod.Val.B{ nLVs( cf)}, [], 2)./ mod.Cal.B( :, nLVs( cf));
%                 stdB( cf) = sum( std( mod.Val.B{ nLV( cf)}, [], 2) );
%                 stdM( cf) = sqrt( sum( mean( mod.Val.B{ nLV( cf)}, 2).^2 ) );
            end
            test = median( abs( test), 2);
            test = test( 2:end)/ test(1);
            nLVs = nLV;
            rmss = rms;
            nLVs( test > opt(5) ) = [];
            rmss( test > opt(5) ) = [];
            temp{ 2} = test;
            
            %There should be some kind of correlation between U and T
            
            
            %In spectroscopic data, the regression coefficients should be
            %decently smooth. Just introducing a very simple test
            test = sign( diff( mod.Cal.B( :, [1 nLV']) ) );
            test = sum( abs( diff( test( :, 2:end) ) ) == 2)/ sum( abs( diff( test( :, 1) ) ) == 2);
            rmsm = rms;
            nLVm( test > opt(6) ) = [];
            rmsm( test > opt(6) ) = [];
            temp{ 3} = test;
                       
            %How much worse than the best are the remaining guesses
            bad = (rms - rms(end) )./ rms(end);
            %AAR 290111 I think no more than about 25% should be accepted
            nLV = nLV( find( bad < opt(2), 1) );
        end
    end
end
if exist( 'nLVx')
    nLV = {nLV, nLVx, nLVh, nLVs, nLVm}; %For debugging
else
    nLV = {nLV};
end
% test = temp;
if nargout < 2
    nLV = nLV{1};
end