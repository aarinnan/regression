function [T,P,R] = pca(X,N)

% [T,P,R] = PCA(X,N)
%           Trekker ut N scorer og ladninger fra X
%           ved hjelp av SVD dekomponering.
%           R er en celle som inneholder N restmatriser
% [T,P] = PCA(X);        
%           gir 2 PCer
%
% Bj�rn Grung

if nargin < 2, N = 2; end

[r,k] = size(X);
if N>r || N>k
    N=min(r,k);
    disp(['The number of components has been reduced to ' num2str(N)])
end
if k < r
  covX = (X'*X)/(r-1);
  [U,S,V] = svd(covX);
else
  covX = (X*X')/(r-1);
  [U,S,V] = svd(covX);
  V = X'*V;
  for i = 1:r
    V(:,i) = V(:,i)/norm(V(:,i));
  end
end

P = V(:,1:N);
T = X*P;

R = cell( N, 1);
for i=1:N
   R{i}=X-T(:,1:i)*P(:,1:i)';
end

%Rotate the solutions so that they always give the same results
s = sign( sum( sign( T) ) );
s( s == 0) = 1;
T = T * diag( s);
P = P * diag( s);