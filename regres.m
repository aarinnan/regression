function model=regres( Xcal, Xval, Ycal, Yval, opt)
% Performs a regression analysis based on a calibration set.
%
%model=regres(Xcal, Xval, Ycal, Yval, opt);
%
% INPUT:
%  Xcal     X-matrix for the calibration set
%  Xval     X-matrix for the validation set
%  Ycal     Y-values for the calibration set
%  Yval     Y-values for the validation set
%  opt      See CV
%
% OUTPUT:
%  model    Predicted Y values

% 080813 AAR Sometimes there are missing values in Xval, but not in Xcal. I
%             have now taken those cases into consideration
% 210908 AAR Make it work with missing values in X
% 080307 AAR Added Paareto scaling (does not work in Y)
% 070107 AAR Make it work even for more predictors than one
% 050107 AAR More outputs, such as predicted T, Xres and b
% 230505 AAR Changed the format of prepro to be two numbers. Also included
%             repeatability and rep+UV (unit variance), but have not
%             implemented it as of yet.
% addpath c:\matlab6p5\mitt\pca

[rY,kY] = size(Ycal);

if length(size(Xcal))~=length(size(Xval)) || size(Xcal,2)~=size(Xval,2) || size(Xcal,3)~=size(Xval,3)
   error('The size of ''Xcal'' and ''Xval'' are not the same')
end
if isempty(Yval)
   Yval = zeros( size( Xval,1), size(Ycal,2));
end
if size(Ycal,2)~=size(Yval,2)
   error('''Ycal'' and ''Yval'' does not have the same number of columns')
end

%Investigate if there are any missing values in X
if sum( isnan( vec( Xcal) ) ) > 0
    ismis = true;
else
    ismis = false;
end

%Pre-processing of data
%X-matrix
Xcp = Xcal;
Xvp = Xval;

if ~ismis
    if length( size( Xcal) ) == 2
        for cp = 1:length( opt.xpp.met)
            [Xcp, Xps] = prepro( Xcp, opt.xpp.met{cp}, opt.xpp.set(cp) );
            Xvp = prepro( Xvp, opt.xpp.met{cp}, Xps );
        end
        %Remove columns which has been set to NaN (from NW/ SG)
        %131014 AAR I sometimes remove the wrong variables. I.e. if there are a
        %            few samples which misses one measurement...
        if any( sum( isnan( Xcp), 1) == size( Xcp, 1)) %|| any( sum( isnan( Xvp), 1) == size( Xvp, 1))
            %         if size( Xvp, 1) == 1
            %             remid = [sum( isnan( Xcp), 1) == size( Xcp, 1); ...
            %                 isnan( Xvp) == 1];
            %         else
            remid = [sum( isnan( Xcp), 1) == size( Xcp, 1); ...
                sum( isnan( Xvp), 1) == size( Xvp, 1)];
            %         end
            remid = sum( remid) > 0;
        else
            remid = false( 1, size( Xcp, 2) );
        end
        
        %Remove columns which do not have any variation in Xcp
        remid( var( Xcp) == 0) = true;
        
        Xcp( :, remid) = [];
        Xvp( :, remid) = [];
        
        %091110 AAR Added 'remid' in output to make it easier in 'cv'
        model.remid = remid;
    else
        [Xcp, Xm, Xs] = nprocess( Xcp, [1 0 0], [0 0 0]); %Only does mean-centering, for now
        Xvp = nprocess( Xvp, [1 0 0], [0 0 0], Xm, Xs);
    end
end

%Y-matrix
Ycp = Ycal;
Yvp = Yval;
if length( size( Ycal) ) == 2
    for cp = 1:length( opt.ypp.met)
        yv = nanvar( Ycp);
        model.Yrem = yv == 0 | isnan( yv);
        [Ycp, Yps] = prepro( Ycp( :, ~model.Yrem), opt.ypp.met{cp}, opt.ypp.set(cp) );
        Yvp = prepro( Yvp( :, ~model.Yrem), opt.ypp.met{cp}, Yps );
    end
    if isempty( opt.ypp.met) %No pre-processing
        Yps.ref = 0;
        Yps.ax = NaN;
        Yps.set = NaN;
    end
else
    error( 'The code cannot handle multiway data at the moment :(')
end

if isempty( Xcp) %In case all variables has been removed
    model = [];
else    
    switch lower( opt.method)
        case 'pls'
            %Investigate if there are missing values in X
            if sum( vec( isnan(Xcp) ) ) > 0 
                if any( sum( isnan( Xcp), 2) == size( Xcp, 2) )
                    error( 'This function cannot handle samples which are completely missing')
                end
                
                if any( sum( isnan( Xcp), 1) == size( Xcp, 1)) 
                    remid = [sum( isnan( Xcp), 1) == size( Xcp, 1); ...
                        sum( isnan( Xvp), 1) == size( Xvp, 1)];
                    remid = sum( remid) > 0;
                else
                    remid = false( 1, size( Xcp, 2) );
                end
                
                %Remove columns which do not have any variation in Xcp
                remid( var( Xcp) == 0) = true;
                
                Xcp( :, remid) = [];
                Xvp( :, remid) = [];
                
                %091110 AAR Added 'remid' in output to make it easier in 'cv'
                model.remid = remid;
                
                if iscell( Yps) %If it is segment-wise pre-processing
                    clear Yps
                    Yps.ref = [0; 1];
                end
                if length( Yps.ref) == 1
                    Yps.ref( 2) = 1;
                end
                popt = plsmiss;
                popt.xpp = opt.xpp;
                popt.ypp = opt.ypp;
                for cf = 1:opt.nLV
                    mod = plsmiss( Xcp, Ycp, cf, popt);
                    Xvpp = prepro( Xvp, popt.xpp.met, mod.xppset{1});

                    %To find the rms0-value to input into plspred
                    Xcpp = Xcp;
                    Xcpp( isnan( Xcpp) ) = mod.Xnew( isnan( Xcpp) );
                    Xcpp = prepro( Xcpp, popt.xpp.met, mod.xppset{1});
                    yc = Xcpp * mod.B * Yps.ref(2) + Yps.ref(1);
                    rms0 = sum( cen_std( Ycal).^2 );
                    [yp, tp, xr] = plspred( Xvpp, mod.B, mod.Px, Yps.ref, rms0, true); %Not totally flexible, but works for now
                    model.yp( :, cf) = yp( :, end);
                    model.Tp{ cf} = tp;
                    if size( xr, 1) == 1 
                        model.Xres( :, cf) = xr( end); %Changed 131010
                    else
                        model.Xres( :, cf) = xr( :, end);
                    end
                    model.B( :, cf) = mod.B( :, end);
                    model.Up{ cf} = Yvp * pinv( mod.Py', 3*eps);
                end
            else %if sum( vec( isnan( Xcp) ) ) > 0
%                 [model.B, w, t, p, u, q] = apls( Xcp, Ycp, opt.nLV, [], [], 1);
                if any( ~isnan( opt.weights.X)) || any( ~isnan( opt.weights.Y) )
                    wopt = wpls;
                    wopt.WX = opt.weights.X;
                    wopt.WY = opt.weights.Y;
                    wopt.tol = [ eps * numel( Xcp) eps * numel( Ycp)];
                    tempmod = wpls( Xcp, Ycp, opt.nLV, wopt);
                else
                    if size( Ycp, 2) == 1
                        tempmod = bipls( Xcp, Ycp, opt.nLV);
                        %                     [model.B, w, t, p, u, q] = bipls( Xcp, Ycp, opt.nLV);
                    else
                        tempmod = simpls( Xcp, Ycp, opt.nLV);
%                         tempmod = apls( Xcp, Ycp, opt.nLV);
                    end
                end
                f = fieldnames( tempmod);
                for cf = 1:length( f)
                    model.( f{cf}) = tempmod.( f{cf} );
                end
                %Predict the calibration set (mainly for classifications)
                model.ycal = plspred( Xcp, model.B, [], Yps.ref);
                [model.yp, model.Tp, model.Xres] = plspred( Xvp, model.B, model.Px, Yps.ref);
                if sum( vec( isnan( Xvp) ) ) > 0
                    for cf = 1:length( model.Tp)
                        model.Up{cf} = Yvp * pinv( model.Py( :, 1:cf)', 3 * eps);
                    end
                else
                    model.Up = Yvp * pinv( model.Py', 3*eps);
                end
            end
        case 'pcr'        
        case 'parafac'
        case 'npls'
            [model.Xfac, model.Yfac, model.Core, model.B] = npls( Xcp, Ycp, opt.nLV, NaN);
            for cf = 1:3
                [model.yp( :, cf), model.Tp, ssx, model.Xres{cf}] = npred( Xvp, cf, model.Xfac, model.Yfac, model.Core, model.B, NaN);
            end
        case 'wpls'
            [model.B, w, t, p, u, q] = wpls( Xcp, opt.weights.X, Ycp, opt.weights.Y, opt.nLV);
            [model.yp, model.Tp, model.Xres] = plspred( Xvp, model.B, p, Yps.ref);
            model.Up = Yvp * pinv( q', 3 * eps);
    end
end
